<?php


	/* Paramétres de connexion à la base de données Mysql */
	$host='localhost';
	$mysql_ident='LOGIN';
	$mysql_mdp='MOTDEPASSE';
	$base='BASE';

	// Change le dossier courant utile lors du lancement des tâches cron
	//chdir(dirname(__FILE__));


	// correspondance  ville=> prefixe tables
        $pref_tab_all = array('VILLE1' => "vi1_", 'VILLE2' => "vi2_");

        // correspondance ville => prefixe certificats
	$cert_all = $pref_tab_all;

	// Mot de passe donné lors de la création des fichiers pem
	$pass="";

	// Url vers la plateforme s2low que vous voulez atteindre
	define('URL', 'https://www.s2low.org/');

	// Identifiant et mdp compte pour chaque ville
	$insee_all = array('VILLE1' => "LOGIN1", 'VILLE2' => "LOGIN2");
	$mdp_all = array('VILLE1' => "MDP1", 'VILLE2' => "MDP2");

	// code SIREN des Villes
	$siren_all = array('VILLE1' => 123456789, 'VILLE2' => 987654321);

	// Clé d'accès admin
	$cle_ctrl='CENSURE';

	//salt pour mdp
	$salt="SELSELSELSELSEL";

	//chemin pour le dossier /key (certificats), en dehors arborescence web
	$certpath= "/opt/publis2low/keys/";

	//param proxy
	$proxyadd="http://ip:port";
	$proxyauth="";

?>
